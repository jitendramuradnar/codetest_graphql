import * as jwt from "jsonwebtoken";
import * as sinon from "sinon";
import { getUserId, IContext } from "./utils";
describe("getUserId", () => {
  let verifyStub;
  beforeEach(() => {
     verifyStub = sinon.stub(jwt, "verify");
});
  afterEach(() => {
    sinon.restore();
   });
  it("should load data from Authorization header", () => {
    const testContext = {} as IContext;
    testContext.request = {};
    testContext.request.get = jest.fn((x) => x);
    expect(() => {
      getUserId(testContext);
    }).toThrowError();
    expect(testContext.request.get).toBeCalled();
    expect(testContext.request.get).toBeCalledWith("Authorization");
  });

  it("should ignore jwt verification without a token", () => {
    const testContext = {} as IContext;
    testContext.request = {
      get: jest.fn((x) => ""),
    };
    expect(() => {
      getUserId(testContext);
    }).toThrow("Not authorized");
    expect(verifyStub.args.length).toBe(0);
  });

  it("should verify with jwt if a token is present", () => {
    const testContext = {} as IContext;
    testContext.request = {
      get: jest.fn((x) => (x === "Authorization" ? "Bearer tokenX" : x)),
    };
    verifyStub.onCall(0).returns({ userId: "U2018" });
    expect(getUserId(testContext)).toBe("U2018");
    // jwt.verify was called once with tokenX as the token param
    expect(verifyStub.args.length).toBe(1);
    expect(verifyStub.getCall(0).args[0]).toBe("tokenX");

  });
});
