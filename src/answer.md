There are following security flaws

1.  It can be run using any value for APP_SECRET like  APP_SECRET="ABC", APP_SECRET="XYZ".
2.  There is no timelimit or session for any login. Once a token is generated it has no session/timelimit.